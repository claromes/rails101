FROM ruby:2.6.3

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /rails101
WORKDIR /rails101

ADD Gemfile /rails101/Gemfile
ADD Gemfile.lock /rails101/Gemfile.lock

RUN bundle install

ADD . /rails101