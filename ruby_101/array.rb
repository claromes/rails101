array_a = [] # start an array
array_a.push(10)
array_a.push(11)
array_a.push("ruby")

p array_a
# [10, 11, "ruby"]

array_b = Array.new # start an array
array_b.push(12)
array_b.push("rails")

p array_b
# [12, "rails"]
p array_b[0]
# 12

array_c = %w(ruby on rails course) # start an array and split the string

p array_c
# ["ruby", "on", "rails", "course"]
p array_c[2]
# "rails"

array_d = %w(10 20 30 40) # start an array and split the string

p array_d
# ["10", "20", "30", "40"]
p array_d[3]
# "40"

#Methods, parameters, block
# To display methods options on bash <array_name>. <TAB>

## Size
p "array_a: "
p array_a.size
p "array_b: "
p array_b.size
p "array_c: "
p array_c.size

## Parameters

p array_a.size.eql?(5)
# false
p array_a.size.eql?(3)
# true

## Block
## Print each element (var = elm) defined in a code block
array_c.each do |elm|
  p elm
end
# "ruby"
# "on"
# "rails"
# "course"

## Print the length of each element/string (var = str) defined in a code block
array_c.each do |str|
 p str.size
end
# 4
# 2
# 5
# 6