hash_1 = {"framework" => "rails", "lang" => "ruby"} # start a hash

p hash_1
# {"framework"=>"rails", "lang"=>"ruby"}
p hash_1["lang"] # search a key
# "ruby"

options = {"font" => "arial", "size" => 12}

p options
# {"font"=>"arial", "size"=>12}
p options["font"]
# "arial"
p options["size"]
# 12