# Mixin - Multiple inheritance
require_relative "module_config"
require_relative "module_user"

class Mixin
  include Config
  include User
end

dev = Mixin.new
dev.linux
# "You are free!"
dev.login
# "You are logged!"