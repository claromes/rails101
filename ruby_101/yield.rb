def print
  p "This is a print"
  yield
  p "This is another print"
end

# print()
# Error
print() {p 'This is the Yield'}
# "This is a print"
# "This is the Yield"
# "This is another print"