require_relative "class" # File with Pessoa class

class Man < Pessoa
  def correr
    p "Correndo..."
  end
end

man = Man.new
man.correr
# "Correndo..."
man.falar
# "Falando..."
p man.class
# Man